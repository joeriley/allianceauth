# Structure Timers

Structure Timers helps you keep track of both offensive and defensive structure timers in your space.

![timerboard](/_static/images/features/apps/timerboard.png)

## Installation

Add `'allianceauth.timerboard',` to your `INSTALLED_APPS` list in your auth project's settings file. Run migrations to complete installation.
